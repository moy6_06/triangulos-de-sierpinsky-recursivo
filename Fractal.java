
package fractal;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;


public class Fractal extends JFrame implements ActionListener {
    
    int  vx1=415, vy1=54,vx2=674, vy2=503,vx3=156,vy3=503;
    private JPanel contentPane;     
    private Rectangle2D bar;
    private JTextField jf= new JTextField();
    private JButton jb = new JButton();
     
     
      
    public Fractal (){
        
        setBounds(100, 100, 820, 570);
        setTitle("Fractal");;
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        this.getContentPane().setBackground(Color.cyan);
        jf.setBounds(0, 500, 50, 20);
        jb.setBounds(50, 500, 100, 20);
        jb.setText("Iteraciones");
        add(jf);
        add(jb);
        jb.addActionListener(this);
        
      }
    
    
    public static void main(String[] args) {
        Fractal f= new Fractal();
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
 
    public void Dibujar(Graphics g){
        int it=Integer.parseInt(jf.getText());
        System.out.println("loiju");
         g.setColor(Color.BLACK);
         g.drawLine(10,10 ,50 ,10);
     
        
    }
    
                                       
   
    private void Fractal(Graphics g,int vx1, int vy1, int vx2, int vy2, int vx3, int vy3,int niv){
        int px1, px2, px3, py1, py2, py3;      
        System.out.println(niv);
        
        g.setColor(Color.black);
            if (niv == 0) {
               g.drawLine(vx1, vy1, vx2, vy2);
               g.drawLine(vx1, vy1, vx3, vy3);
               g.drawLine(vx3, vy3, vx2, vy2);
            }
            
            else {
                //-----Primer punto medio--------//
                px1 = (int) ((vx1 + vx2) / 2);
                py1 = (int) ((vy1 + vy2) / 2);
                //------Segundo punto medio-------//
                px2 = (int) ((vx1 + vx3) / 2);
                py2 = (int) ((vy1 + vy3) / 2);
                //-----Tercer Punto medio--------//
                px3 = (int) ((vx2 + vx3) / 2);
                py3 = (int) ((vy2+ vy3) / 2);

                //-----------Llamado recursivo con nuevos puntos---------//
                Fractal(g, vx1, vy1, px1, py1, px2, py2, niv - 1);
                Fractal(g, px1, py1, vx2, vy2, px3, py3, niv - 1);
                Fractal(g, px2, py2, px3, py3, vx3, vy3, niv - 1);
            }
            
            
}
   
    
    /*private void paintComponent(Graphics g){
        System.out.println("hola");
         bar = new Rectangle2D.Double(470,570,120,10);
         Graphics2D g2D = (Graphics2D)g;
         g2D.setBackground(Color.LIGHT_GRAY);
         g2D.setColor(Color.BLACK);
         g2D.draw(bar);
    }*/

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if(ae.getSource()==jb){
            //Dibujar(contentPane.getGraphics());
            System.out.println("entro");
        Fractal(contentPane.getGraphics(), vx1, vy1, vx2, vy2, vx3, vy3,Integer.parseInt(jf.getText()) );
        }
            
    }
    
    
}   
   /*  private void paintRecursivo(Graphics g,int vx1, int vy1, int vx2, int vy2, int vx3, int vy3,int niv){
        int px1, px2, px3, py1, py2, py3;       
        niv=3;
        /*int R = (int) (Math.random() * 256) + 0;
        int G = (int) (Math.random() * 256) + 0;
        int B = (int) (Math.random() * 256) + 0;*/
        /*try {
            //Color color = new Color(R, G, B);
            //g.setColor(color);
            g.setColor(Color.black);
            if (niv == 0) {
               g.drawLine(vx1, vy1, vx2, vy2);
               g.drawLine(vx1, vy1, vx3, vy3);
               g.drawLine(vx3, vy3, vx2, vy2);
            }
            
            else {
                //-----Primer punto medio--------//
                px1 = (int) ((vx1 + vx2) / 2);
                py1 = (int) ((vy1 + vy2) / 2);
                //------Segundo punto medio-------//
                px2 = (int) ((vx1 + vx3) / 2);
                py2 = (int) ((vy1 + vy3) / 2);
                //-----Tercer Punto medio--------//
                px3 = (int) ((vx2 + vx3) / 2);
                py3 = (int) ((vy2+ vy3) / 2);

                //-----------Llamado recursivo con nuevos puntos---------//
                paintRecursivo(g, vx1, vy1, px1, py1, px2, py2, niv - 1);
                paintRecursivo(g, px1, py1, vx2, vy2, px3, py3, niv - 1);
                paintRecursivo(g, px2, py2, px3, py3, vx3, vy3, niv - 1);
            }
          //  Thread.sleep(100);

        } catch (Exception e) {
            e.printStackTrace();
        }
            
     }
   
    
}


/*                 800  ,  600

                    400,350


             350,400         450,400
                    
*/